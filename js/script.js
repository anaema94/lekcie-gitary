function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
    document.getElementById("mySidenav").style.height = "350px";
    document.querySelector(".cover").style.backgroundColor = "#666";
    document.getElementById("mySidenav").style.backgroundColor = "rgba(0, 0, 0, 0.8)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.querySelector(".cover").style.marginLeft = "0";
    document.getElementById("mySidenav").style.backgroundColor = "rgba(0, 0, 0, 0.2)";
}

var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
if (isIOS) {
    document.querySelector(".cover").style.backgroundAttachment = "scroll";
    document.querySelector("#references").style.backgroundAttachment = "scroll";
    document.querySelector("#contact-form").style.backgroundAttachment = "scroll";
} else {}